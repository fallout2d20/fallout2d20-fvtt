/*
PointPoolUI for Fallout 2d20
by Emilio Gonzalez

Manages and displays any number of PointPools for use in a multiplayer Foundry VTT environment.

This requires:
- The PointPool class, pointpool.js
- An actionpoints-app.html template and supporting CSS
- Localized strings in the appropriate files

Once you have created PointPools, pass an array of them into a new PointPoolUI, such as:
  game.pointPoolUI = new PointPoolUI([game.partyAP, game.gameAP, game.bossAP]);

Then, manipulate PointPools in the UI or by passing them into PointPoolUI:
  game.pointPoolUI.incrementPool(game.gameAP);
 */
 
export default class PointPoolUI extends Application {

  constructor(pools = []) {
    super();
    
    this.pools = pools;    
    this.eventSystemName = 'system.' + game.system.id;
  }
  
  /* 

  config the Application class UI
  
 */
  static get defaultOptions() {
    const appOptions = super.defaultOptions;
    appOptions.template = 'systems/fallout2d20/templates/actionpoints-app.html';
    appOptions.popOut = true;
    appOptions.minimizable = true;
    appOptions.resizeable = false;
    appOptions.id = "PointPoolUI";
    appOptions.width = '180';
    appOptions.height = 'auto';
    return appOptions;
  }
  
  /* 

  define the Application data object to render
  
 */
  async getData() {
    let appData = super.getData();
    
    appData.pools = {};
    
    // retrieve values
    for (const pool of this.pools) {
      pool.readSettings();
      
      // create the output object
      let outputObject = {
        name: pool.name,
        title: game.i18n.localize(pool.title),
        points: pool.points, 
        min: pool.min, 
        max: pool.max, 
        isHidden: !game.user.isGM && (pool.isHidden && !game.settings.get(game.system.id, 'PlayersCanSeeHiddenPools')),
        canEdit: game.user.isGM || (game.settings.get(game.system.id, 'PlayersCanEditPools') && !pool.isGMOnly)
      };
      appData.pools[pool.name] = outputObject;
    }
    
    return appData;
  }

/* 

  listen for UI interaction
  
 */
  activateListeners(html) {
    super.activateListeners(html);
    
    for (const pool of this.pools) {
      // listen for a numeric input
      html.find('.' + pool.name + '-points-field').change(event => {
        this.setPool(pool, event.target.value);
      });
    
      // listen for decrement
      html.find('.' + pool.name + '-dec').click(event => {
        this.decrementPool(pool);
      });
    
      // listen for increment
      html.find('.' + pool.name + '-inc').click(event => {
        this.incrementPool(pool);
      });
    
      // listen for min change
      html.find('.' + pool.name + '-min').change(event => {
        this.setPoolMin(pool, event.target.value);
      });
    
      // listen for max change
      html.find('.' + pool.name + '-max').change(event => {
        this.setPoolMax(pool, event.target.value);
      });
    
      // listen for reset
      html.find('.' + pool.name + '-reset').click(event => {
        this.resetPool(pool);
      });
      
      // listen for the panel toggle
      html.find('.' + pool.name + '-details-toggle').click(event => {
        let panel = html.find('.' + pool.name + '-details')[0];
        let link = html.find('.' + pool.name + '-details-show')[0];
        
        // toggle display
        if (panel.style.display == 'none') {
          panel.style.display = 'block';
          link.style.display = 'none';
        }
        else {
          panel.style.display = 'none';
          link.style.display = 'inline';
        }
        
        // redraw to adapt the app window size
        this.setPosition();
      });
    }
  }
  
  /* 

  set the pool to a specific value
  
 */
  async setPool(pool, value) {
    if (game.user.isGM) {
      await pool.setPoints(value);
      game.socket.emit(this.eventSystemName, {type: 'refreshPointPools'});
      this.render();
    }
    else {
      game.socket.emit(this.eventSystemName, {type: 'setPointPool', pool: pool.name, value: value});
    }
  }
  
  /* 

  increment or decrement the pool by a value
  
 */
  async modifyPool(pool, value) {
    if (game.user.isGM) {
      await pool.modifyPoints(value);
      game.socket.emit(this.eventSystemName, {type: 'refreshPointPools'});
      this.render();
    }
    else {
      game.socket.emit(this.eventSystemName, {type: 'modifyPointPool', pool: pool.name, value: value});
    }
  }
  
  /* 

  decrement the pool by 1
  
 */
  async decrementPool(pool) {
    if (game.user.isGM) {
      await pool.modifyPoints(-1);
      game.socket.emit(this.eventSystemName, {type: 'refreshPointPools'});
      this.render();
    }
    else {
      game.socket.emit(this.eventSystemName, {type: 'modifyPointPool', pool: pool.name, value: -1});
    }
  }
  
  /* 

  increment the pool by 1
  
 */
  async incrementPool(pool) {
    if (game.user.isGM) {
      await pool.modifyPoints(1);
      game.socket.emit(this.eventSystemName, {type: 'refreshPointPools'});
      this.render();
    }
    else {
      game.socket.emit(this.eventSystemName, {type: 'modifyPointPool', pool: pool.name, value: 1});
    }
  }
  
  /* 

  set the min value
  
 */
  async setPoolMin(pool, value) {
    if (game.user.isGM) {
      await pool.setMin(value);
      game.socket.emit(this.eventSystemName, {type: 'refreshPointPools'});
      this.render();
    }
    else {
      game.socket.emit(this.eventSystemName, {type: 'setPointPoolMin', pool: pool.name, value: value});
    }
  }
  
  /* 

  set the max value
  
 */
  async setPoolMax(pool, value) {
    if (game.user.isGM) {
      await pool.setMax(value);
      game.socket.emit(this.eventSystemName, {type: 'refreshPointPools'});
      this.render();
    }
    else {
      game.socket.emit(this.eventSystemName, {type: 'setPointPoolMax', pool: pool.name, value: value});
    }
  }
  
  /* 

  reset the pool to its constructor's defaults
  
 */
  async resetPool(pool) {
    if (game.user.isGM) {
      await pool.reset();
      game.socket.emit(this.eventSystemName, {type: 'refreshPointPools'});
      this.render();
    }
    else {
      game.socket.emit(this.eventSystemName, {type: 'resetPointPool', pool: pool.name});
    }
  }
}

/* 

run at Foundry init

 */
Hooks.once('init', () => {
  // register system settings
  // players can edit visible pools
  game.settings.register(game.system.id, "PlayersCanEditPools", {
    name: "PointPool.PlayersCanEditPools_Name",
    hint: "PointPool.PlayersCanEditPools_Hint",
    scope: "world",
    type: Boolean,
    default: true,
    config: true
  });
  // players can see GM pools
  game.settings.register(game.system.id, "PlayersCanSeeHiddenPools", {
    name: "PointPool.PlayersCanSeeHiddenPools_Name",
    hint: "PointPool.PlayersCanSeeHiddenPools_Hint",
    scope: "world",
    type: Boolean,
    default: false,
    config: true
  });
});

/* 

run at Foundry ready

 */
Hooks.on('ready', () => {
  // set up socket listeners for PointPoolUI changes from other clients
  game.socket.on('system.' + game.system.id, event => {
    // console.log(event);
    // listen for party AP changes
    if (event.type == 'setPointPool' && game.user.isGM) {
      console.log('received setPointPool');
      let eventPool = game[event.pool];
      game.pointPoolUI.setPool(eventPool, event.value);
    }
    else if (event.type == 'modifyPointPool' && game.user.isGM) {
      console.log('received modifyPointPool');
      let eventPool = game[event.pool];
      game.pointPoolUI.modifyPool(eventPool, event.value);
    }
    else if (event.type == 'setPointPoolMin' && game.user.isGM) {
      console.log('received setPointPoolMin');
      let eventPool = game[event.pool];
      game.pointPoolUI.setPoolMin(eventPool, event.value);
    }
    else if (event.type == 'setPointPoolMax' && game.user.isGM) {
      console.log('received setPointPoolMax');
      let eventPool = game[event.pool];
      game.pointPoolUI.setPoolMax(eventPool, event.value);
    }
    else if (event.type == 'resetPointPool') {
      console.log('received resetPointPool');
      let eventPool = game[event.pool];
      game.pointPoolUI.resetPool(eventPool);
    }
    else if (event.type == 'refreshPointPools') {
      console.log('received refreshPointPools');
      game.pointPoolUI.render();
    }
  });
  
  // start the PointPoolUI
  game.pointPoolUI.render(true);
});