/* 

PointPool
by Emilio Gonzalez

A class to track public and GM point pools in Foundry VTT.  This is not a UI class, it only handles numbers.

Generally, you just want to create PointPools and pass them to PointPoolUI; modifying PointPools during play should only be handled through PointPoolUI.

This is intended for use with Modiphius 2d20 systems, like Action Points in Fallout or Momentum in Conan.  It is loosely based on the Conan and Dishonored Foundry system implementations.

poolName:  the internal name of the pool's object.
poolTitle:  the human-readable, localized name of the pool.
poolMin:  the minimum value, usually 0.
poolMax:  the maximum value.
poolIsHidden:  only GMs can see the pool
poolIsGMOnly:  only GMs can edit the pool
*/

export default class PointPool {
/* 

  initialize the pool's values

 */
  constructor(poolName, poolTitle, poolMin, poolMax, poolIsHidden, poolIsGMOnly) {
    // get the current game system's name
    this.systemName = game.system.id;

    // create local metadata vars
    this.name = poolName;
    this.title = poolTitle;
    this.isHidden = poolIsHidden;
    this.isGMOnly = poolIsGMOnly;
    
    // create local data vars
    if (poolMin < 0) poolMin = 0;  // point pools can't be negative
    this.min = poolMin;
    this.max = poolMax;
    this.points = 0;
    
    // register settings
    game.settings.register(this.systemName, this.name, {
      scope: 'world',
      config: false,
      type: Number,
      default: 0
    });
    game.settings.register(this.systemName, this.name + 'Min', {
      scope: 'world',
      config: false,
      type: Number,
      default: poolMin
    });
    game.settings.register(this.systemName, this.name + 'Max', {
      scope: 'world',
      config: false,
      type: Number,
      default: poolMax
    });
    game.settings.register(this.systemName, this.name + 'IsHidden', {
      scope: 'world',
      config: false,
      type: Boolean,
      default: poolIsHidden
    });
    game.settings.register(this.systemName, this.name + 'IsGMOnly', {
      scope: 'world',
      config: false,
      type: Boolean,
      default: poolIsGMOnly
    });
    
    // update local vars in case there were already settings
    this.readSettings();
  }


/* 

  set the point pool to a value

 */
  async setPoints(value) {
    // update current values
    this.readSettings();

    // validate input
    try {
      this.validateInput(value);
    }
    catch (e) {
      console.log(e);
      return;
    }
    
    // round input
    value = Math.round(value);
    
    // cap value at min or max
    if (value < this.min) { value = this.min; }
    else if (value > this.max) { value = this.max; }
    
    this.points = value;
    
    // write to the game settings
    await this.writeSettings();
  }
  
  
/* 

  change a point pool by a value

 */
  async modifyPoints(diff) {
    // update current values
    this.readSettings();
    
    // validate input
    try {
      this.validateInput(diff);
    }
    catch (e) {
      console.log(e);
      return;
    }
    
    // round input
    diff = Math.round(diff);
    
    // cap value at min or max
    if ((diff + this.points) < this.min) {
      this.points = this.min; 
    } else if ((diff + this.points) > this.max) {
      this.points = this.max;
    }
    else {
      this.points = diff + this.points;
    }
    
    // write to the game settings
    await this.writeSettings();
  }

/* 

  set the minimum to a value

 */
  async setMin(value) {
    // update current values
    this.readSettings();
    // console.log(this.toString() + " new min " + value);

    // validate input
    try {
      this.validateInput(value);
    }
    catch (e) {
      console.log(e);
      return;
    }
    
    // process input
    value = Math.round(value); // pools must be ints
    if (value < 0) { value = 0; } // pools can't be less than zero
    else if (value > this.max) { value = this.max; } // min can't be more than max
    
    this.min = value;
    
    // make sure the current point value conforms to the new min
    if (this.points < this.min) { this.points = this.min; }
    
    // write to the game settings
    await this.writeSettings();
  }

/* 

  set the maximum to a value

 */
  async setMax(value) {
    // update current values
    this.readSettings();
    // console.log(this.toString() + " new max " + value);

    // validate input
    try {
      this.validateInput(value);
    }
    catch (e) {
      console.log(e);
      return;
    }
    
    // process input
    value = Math.round(value); // pools must be ints
    if (value > Number.MAX_SAFE_INTEGER) { value = Number.MAX_SAFE_INTEGER; } // max can't be absurd, just ridiculous
    else if (value < this.min) { value = this.min; } // max can't be less than min
    
    this.max = value;
    
    // make sure the current point value conforms to the new max
    if (this.points > this.max) { this.points = this.max; }
    
    // write to the game settings
    await this.writeSettings();
  }


/* 

  make sure text input is acceptable

 */
  validateInput(input) {
    // define error messages
    const validationError = game.i18n.localize('PointPool.InputError');

    // console.log("validating " + input);
    
    // not a number
    if (isNaN(input)) {
      // throw error
      console.log(validationError);
      ui.notifications.error(validationError);
      throw validationError;
    }
  }

/* 

  load settings into local vars

 */
  readSettings() {
    // debugger;
    this.points = game.settings.get(this.systemName, this.name);
    this.min = game.settings.get(this.systemName, this.name + 'Min');
    this.max = game.settings.get(this.systemName, this.name + 'Max');
    this.isHidden = game.settings.get(this.systemName, this.name + 'IsHidden');
    this.isGMOnly = game.settings.get(this.systemName, this.name + 'IsGMOnly');
  }
  
/* 

  write the local vars to settings

 */

// with await
  async writeSettings() {
    // debugger;
    try {
      await game.settings.set(this.systemName, this.name, this.points);
      await game.settings.set(this.systemName, this.name + 'Min', this.min);
      await game.settings.set(this.systemName, this.name + 'Max', this.max);
      await game.settings.set(this.systemName, this.name + 'IsHidden', this.isHidden);
      await game.settings.set(this.systemName, this.name + 'IsGMOnly', this.isGMOnly);
    }
    catch (e) {
      throw e;
    }
  }
  
/*

  reset all values to default

 */
 
  async reset() {
    try {
      await game.settings.set(this.systemName, this.name, 
                              game.settings.settings.get(this.systemName + "." + this.name).default);
      await game.settings.set(this.systemName, this.name + 'Min', 
                              game.settings.settings.get(this.systemName + "." + this.name + 'Min').default);
      await game.settings.set(this.systemName, this.name + 'Max', 
                              game.settings.settings.get(this.systemName + "." + this.name + 'Max').default);
      await game.settings.set(this.systemName, this.name + 'IsHidden', 
                              game.settings.settings.get(this.systemName + "." + this.name + 'IsHidden').default);
      await game.settings.set(this.systemName, this.name + 'IsGMOnly', 
                              game.settings.settings.get(this.systemName + "." + this.name + 'IsGMOnly').default);
    }
    catch (e) {
      throw e;
    }
    this.readSettings();
  }

/* 

  summarize this pool's current values

 */
  toString() {
    let output = 'PointPool: ' + this.name + ' / ' + this.points + ' / ' 
              + this.min + '-' + this.max + ' / '
              + 'hidden = ' + this.isHidden + ' / '
              + 'GM only = ' + this.isGMOnly;
    
    return output;
  }

/* 

  summarize any pool's stored values

 */
  static settingsToString(system, pool) {
    let output = 'PointPool from settings: ' + pool + ' / ';
    
    output += game.settings.get(system, pool) + ' / ';
    output += game.settings.get(system, pool + 'Min') + '-';
    output += game.settings.get(system, pool + 'Max') + ' / ';
    output += game.settings.get(system, pool + 'IsHidden') + ' / ';
    output += game.settings.get(system, pool + 'IsGMOnly');
    
    return output;
  }

/*

  setters
  non functional!

  set points(value) { this.setPoints(value); }
  set min(value) { this.setMin(value); }
  set max(value) { this.setMax(value); }
*/

}

/* 

  test pool function
  
  requires a pool object
  
*/
window.testPool=async function(pool) {
  // pool testing for normal Fallout 2d20 party AP with a range of 0-6
  console.log(PointPool.settingsToString(game.system.id, pool.name));
  
  console.log(game.settings.get(game.system.id, pool.name));
  
  await pool.setPoints(4);
  
  // points should be 4
  console.log(pool.toString());
  console.log(PointPool.settingsToString(game.system.id, pool.name));
  
  console.log(game.settings.get(game.system.id, pool.name));
  
  await pool.modifyPoints(1);
  
  // points should be 5
  console.log(pool.toString());
  console.log(PointPool.settingsToString(game.system.id, pool.name));
  
  console.log(game.settings.get(game.system.id, pool.name));
  
  await pool.modifyPoints(2);
  
  // points should be 6 (5+2 but 6 max)
  console.log(pool.toString());
  console.log(PointPool.settingsToString(game.system.id, pool.name));
  
  console.log(game.settings.get(game.system.id, pool.name));
  
  await pool.modifyPoints(-3);
  
  // points should be 3
  console.log(pool.toString());
  console.log(PointPool.settingsToString(game.system.id, pool.name));
  
  console.log(game.settings.get(game.system.id, pool.name));
  
  await pool.modifyPoints(-4);
  
  // points should be 0 (3-4 but 0 min)
  console.log(pool.toString());
  console.log(PointPool.settingsToString(game.system.id, pool.name));
  
  console.log(game.settings.get(game.system.id, pool.name));
  
  // randomize
  await pool.setPoints(Math.round(MersenneTwister.random() * pool.max));
  console.log(game.settings.get(game.system.id, pool.name));
}