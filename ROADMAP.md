# Fallout 2d20 development goals

The primary goal of this project is to facilitate playing *Fallout TRG* with the Foundry VTT.  Once that is complte, the secondary goal is to host the game system.

## Crawl: a tabletop replacement

Provide features for basic virtual tabletop play of *Fallout TRG* using external character sheets.

- Track action points for the players and GM
    - A group AP counter visible to all users
    - Group control over the AP counter
    - An identical counter for the GM's pool, only visible to the GM
- Track HP and luck for tokens
- Set of example NPCs
- Rough data models for SPECIAL and NPC sheets, but no UI
    - Google Sheets sync?

## Walk: doing the math

Support a more complete character sheet and the appropriate rolls.

- SPECIAL character sheets
    - Limb condition
    - Calculate DR
- NPC character sheets
    - Abilities
- Rolls for skills, attacks, and damage
- Attacks on sheets
    - Set weapon type & ranges for attack rolls
    - Damage types
    - Track weapon attributes
    - Basic weapon DB?  Complication tables?
- Armor on sheets
    - Limb coverage
    - Damage resistance & type
- Alternate grid rules
- Update example NPCs for new features
- Set of example maps, tiles, SFX, etc.
- Set of example scenes
    - Build one or more random encounters from the rulebook

## Run: a complete presentation

Polished play and UI for a more stylized experience.

- Custom UI
    - CSS for dialog, chat, etc.
    - Stylized character sheet
    - Journal backgrounds:  terminal, Pip-Boy, paper
    - Fallout-styled effects, statuses, etc. (Vault Boy)
- Databases for common NPCs, weapons, and items
- Scene-structured play
    - Scene-based effects, abilities, etc.
    - GM controls to begin, end, and reset scenes

## Fly: the vat of FEV

Sky's-the-limit ideas for full integration and enhancement.

- Full zone support in Foundry
    - Target selection with difficulty calculation
    - Movement restriction
- Localization
    - EFIGS + Russian + Japanese
