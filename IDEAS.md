# Idea backlog backlog

## Interface

- **Journals**
    - (WALK, RUN) Add a style option to journals to present them in different universe-appropriate formats
        - Terminal, for computers and holotapes
        - Paper, for notes
        - Printout, for documents
        - Normal, for lore or GM notes
    - (RUN, FLY) Interactive journals
        - Allow linking between journals, and the ability to redraw them in the same window
        - Would enable players to "browse" terminals and books
- **Action Points**
    - (WALK) Repurpose Dice So Nice to animate the use of AP
        - Create Nuka-Cola bottlecap skins for the DSN "d2" coin model
        - Add those d2s to rolls, but don't actually count the results
        - Custom bottlecap sound effect?
