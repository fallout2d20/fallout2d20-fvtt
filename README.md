This is the home of the unofficial *Fallout: The Roleplaying Game* system for Foundry VTT.

You might want to [see the roadmap](ROADMAP.md)

### Disclaimer
This is an **unofficial** community-based project.  This project is not a product of ZeniMax Media Inc., Modiphius Entertainment Ltd., Foundry Gaming, LLC, or Vault-Tec Inc.; any support or materials provided by those companies is provided without liability and for non-commercial use only.
